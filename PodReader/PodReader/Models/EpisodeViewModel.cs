﻿namespace PodReader.Models
{
    public class EpisodeViewModel
    {
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? PublishDate {  get; set; }
        public string? Url { get; set; }
    }
}
