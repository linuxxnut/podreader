﻿namespace PodReader.Models
{
    public class PodcastViewModel
    {
        public int? Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? Url { get; set; }
        public DateTime? PublishDate { get; set; }

        public List<EpisodeViewModel>? Episodes { get; set; }
        public int UnreadCount { get; set; }
    }
}
