﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PodReader.Data;
using PodReader.Models;
using System.Diagnostics;
using System.Xml;

namespace PodReader.Controllers
{
    public class HomeController : Controller
    {
        static readonly HttpClient client = new HttpClient();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var ctx = new PodReaderContext();
            var podcasts = ctx.Podcasts
                .Include(x => x.Episodes)
                .OrderByDescending(x => x.PublishDate)
                .Select(y => new PodcastViewModel
                {
                    Id = y.Id,
                    Description = y.Description,
                    PublishDate = y.PublishDate,
                    Title = y.Title,
                    Url = y.Url,
                    UnreadCount = y.Episodes.Count(p => !p.Read)
                }).ToList();

            return View(podcasts);
        }

        public IActionResult Podcast(int? id, int skip = 0, int take = 6)
        {
            var ctx = new PodReaderContext();
            var podcast = ctx.Podcasts.Where(x => x.Id ==  id).FirstOrDefault();
            var episodes = ctx.Episodes.Where(x => x.PodcastId == id)
                .Skip(skip).Take(take).ToList();

            podcast.Episodes = episodes;

            return View(podcast);
        }

        public IActionResult Read(int? id)
        {
            var ctx = new PodReaderContext();
            var episode = ctx.Episodes.FirstOrDefault(x => x.Id == id);

            if (episode != null)
            {
                episode.Read = true;
                ctx.Update(episode);
                ctx.SaveChanges();
            }

            return Ok();
        }

        public IActionResult ReadAll(int? id)
        {
            var ctx = new PodReaderContext();
            var episodes = ctx.Episodes.Where(x => x.PodcastId == id);

            foreach (var episode in episodes) 
            {
                episode.Read = true;
                ctx.Update(episode);
            }

            ctx.SaveChanges();
            return RedirectToAction("Podcast", new { id });
        }

        public IActionResult GetTime(int podId, int epId)
        {
            var ctx = new PodReaderContext();
            var episode = ctx.Episodes.FirstOrDefault(x => x.PodcastId == podId && x.Id == epId);

            return Ok(episode.Seconds);
        }

        public IActionResult SaveTime(int podId, int epId, double time)
        {
            var ctx = new PodReaderContext();
            var episode = ctx.Episodes.FirstOrDefault(x => x.PodcastId == podId && x.Id == epId);
            episode.Seconds = time;

            ctx.Update(episode);
            ctx.SaveChanges();

            return Ok();
        }

        public IActionResult Refresh()
        {
            var ctx = new PodReaderContext();
            var podcasts = ctx.Podcasts.ToList();

            foreach (var podcast in podcasts)
            {
                using HttpResponseMessage response = client.GetAsync(podcast.Url).Result;

                var newEp = new Episode();
                newEp.PodcastId = podcast.Id;
                bool episodes = false;

                XmlReaderSettings settings = new XmlReaderSettings();
                using (XmlReader reader = XmlReader.Create(response.Content.ReadAsStream(), settings))
                {

                    while (reader.Read())
                    {
                        if (reader.Name == "item")
                        {
                            episodes = true;
                            newEp = new Episode
                            {
                                PodcastId = podcast.Id
                            };
                        }

                        if (reader.Name == "title")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                newEp.Title = reader.Value;
                        }

                        if (reader.Name == "description")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.CDATA || reader.NodeType == XmlNodeType.Text)
                                newEp.Description = reader.Value;
                        }

                        if (reader.Name == "pubDate")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                newEp.PublishDate = DateTime.Parse(reader.Value);
                        }

                        if (reader.Name == "enclosure")
                        {
                            newEp.Url = reader.GetAttribute("url");
                        }

                        if (episodes)
                        {
                            if (newEp.PublishDate > DateTime.MinValue)
                            {
                                var oldEp = ctx.Episodes.FirstOrDefault(x => x.PublishDate == newEp.PublishDate);

                                if (podcast.PublishDate < newEp.PublishDate)
                                {
                                    podcast.PublishDate = newEp.PublishDate;
                                    ctx.Update(podcast);
                                }

                                if (oldEp == null)
                                {
                                    ctx.Add(newEp);
                                    episodes = false;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                ctx.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}