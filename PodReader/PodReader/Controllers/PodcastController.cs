﻿using Microsoft.AspNetCore.Mvc;
using PodReader.Data;
using System.Xml;

namespace PodReader.Controllers
{
    public class PodcastController : Controller
    {
        static readonly HttpClient client = new HttpClient();

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddPost(string url)
        {
            var ctx = new PodReaderContext();
            using HttpResponseMessage response = client.GetAsync(url).Result;

            var pod = new Podcast();
            pod.Url = url;

            XmlReaderSettings settings = new XmlReaderSettings();
            using (XmlReader reader = XmlReader.Create(response.Content.ReadAsStream(), settings))
            {

                while (reader.Read())
                {
                    if (reader.Name == "item")
                        break;

                    if (reader.Name == "title")
                    {   
                        reader.Read();
                        if (reader.NodeType == XmlNodeType.Text)
                            pod.Title = reader.Value;
                    }

                    if (reader.Name == "description")
                    {
                        reader.Read();
                        if (reader.NodeType == XmlNodeType.Text)
                            pod.Description = reader.Value;
                    }

                    if (reader.Name == "pubDate")
                    {
                        reader.Read();
                        if (reader.NodeType == XmlNodeType.Text)
                            pod.PublishDate = DateTime.Parse(reader.Value);
                    }
                }

                ctx.Add(pod);
                ctx.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }

        public IActionResult Remove(int id)
        {
            var ctx = new PodReaderContext();
            var pod = ctx.Podcasts.FirstOrDefault(x => x.Id == id);

            if (pod != null)
            {
                ctx.Podcasts.Remove(pod);
                ctx.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
