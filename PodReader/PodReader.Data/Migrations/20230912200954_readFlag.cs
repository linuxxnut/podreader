﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PodReader.Data.Migrations
{
    /// <inheritdoc />
    public partial class readFlag : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Read",
                table: "Episodes",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Read",
                table: "Episodes");
        }
    }
}
