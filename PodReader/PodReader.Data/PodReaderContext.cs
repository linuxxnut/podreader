﻿using Microsoft.EntityFrameworkCore;

namespace PodReader.Data
{
    public class PodReaderContext : DbContext
    {
        public DbSet<Podcast> Podcasts { get; set; }
        public DbSet<Episode> Episodes { get; set; }

        public string DbPath { get; }

        public PodReaderContext()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = System.IO.Path.Join(path, "podReader.db");
        }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
    }

    public class Podcast
    {
        public int? Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? Url { get; set; }
        public DateTime? PublishDate { get; set; }

        public IEnumerable<Episode>? Episodes { get; set; }
    }

    public class Episode
    {
        public int? Id { get; set; }
        public bool Read { get; set; }
        public int? PodcastId { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public string? Url { get; set; }
        public double? Seconds { get; set; }
    }
}